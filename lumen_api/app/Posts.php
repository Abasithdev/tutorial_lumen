<?php
/**
 * Created by PhpStorm.
 * User: RedBorn
 * Date: 28/10/2017
 * Time: 15.46
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['title','body','views'];
}
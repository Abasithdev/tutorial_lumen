<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
//    return "check ".(1+1);
    return $app->version();
});

//nesting group pada lumen tidak bisa ternyata :D
$app->group(['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers'],function() use ($app){

        $app->post('post/add','PostsController@createPost');
        $app->get('post/view/{id}','PostsController@viewPost');
        $app->put('post/edit/{id)','PostsController@updatePost');
        $app->delete('post/delete/{id}','PostsController@deletePost');
        $app->get('post/index','PostsController@index');

        $app->get('user/index','UsersController@index');
        $app->get('user/view','UsersController@view');
        $app->put('user/edit','UsersController@edit');
        $app->post('user/add','UsersController@add');
        $app->delete('user/delete','UsersController@delete');

});


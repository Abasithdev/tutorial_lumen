<?php
/**
 * Created by PhpStorm.
 * User: RedBorn
 * Date: 28/10/2017
 * Time: 15.54
 */

namespace App\Http\Controllers;

use App\Posts;
use App\Http\Controllers;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function index(){

        $post = Posts::all();
        return response()->json($post);
    }

    public function createPost(Request $request){
        $post = Posts::create($request->all());
        return response()->json($post);
    }

    public function viewPost($id){
        $post = Posts::find($id);

        return response()->json($post);
    }

    public function updatePost(Request $request, $id){

        $post = Posts::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->views = $request->input('views');
        $post->save();

        return response()->json($post);
    }

    public function deletePost($id){
        $post = Posts::find($id);
        $post->delete();
        return response()->json($post);
    }
}
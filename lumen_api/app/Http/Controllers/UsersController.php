<?php
/**
 * Created by PhpStorm.
 * User: RedBorn
 * Date: 28/10/2017
 * Time: 17.37
 */

namespace App\Http\Controllers;

use App\User;
use App\Users;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;

class UsersController extends Controller
{

    public function index(){
        $user = User::all();
        return response()->json($user);
    }

    public function add(Request $request){
        $request['api_token'] = str_random(60);
        $request['password'] = app('hash')->make($request['password']);
        $user = User::create($request->all());

        return response()->json($user);
    }

    public function edit(Request $request, $id){
        $user = User::find($id);
        $user->update($request->all());

        return response()->json($user);
    }

    public function view($id){
        $user = User::find($id);
        return response()->json($user);
    }

    public function delete($id){
        $user = User::find($id);
        $user->delete();

        return response()->json($user);
    }
}